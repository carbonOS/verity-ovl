// Recursively measures the fsverity digest of each
// file in the directory provided
//
// To generate the fsverity hashes:
// find /path/to/dir -type f -exec fsverity enable {} \;
//
// Then to recursively measure:
// ./verity-test /path/to/dir

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <dirent.h>
#include <sys/ioctl.h>
#include <sys/stat.h> 
#include <fcntl.h>
#include <linux/fsverity.h>
#include <unistd.h>

#define PRINT 0
#define FS_VERITY_MAX_DIGEST_SIZE 64

static char
bin2hex_char(uint8_t nibble)
{
	if (nibble < 10)
		return '0' + nibble;
	return 'A' + (nibble - 10);
}

static void
bin2hex(const uint8_t *bin, size_t bin_len, char *hex)
{
	size_t i;
	for (i = 0; i < bin_len; i++) {
		*hex++ = bin2hex_char(bin[i] >> 4);
		*hex++ = bin2hex_char(bin[i] & 0xf);
	}
	*hex = '\0';
}

static inline void*
zalloc(size_t size)
{
	return memset(malloc(size), 0, size);
}

static void
handle_file(char *path)
{		
	int fd = open(path, O_RDONLY, 0);
	if (fd < 0) {
		fprintf(stderr, "ERROR: Failed to open file %s: %m\n", path);
		exit(EXIT_FAILURE);
	}

	struct fsverity_digest *d = NULL;
	d = zalloc(sizeof(*d) + FS_VERITY_MAX_DIGEST_SIZE);
	d->digest_size = FS_VERITY_MAX_DIGEST_SIZE;
	
	if (ioctl(fd, FS_IOC_MEASURE_VERITY, d) != 0) {
		fprintf(stderr, "ERROR: MEASURE_VERITY failed for path %s: %m\n", path);
		exit(EXIT_FAILURE);
	}

#if PRINT
	// Print the file's digest
	char digest_hex[FS_VERITY_MAX_DIGEST_SIZE * 2 + 1];
	bin2hex(d->digest, d->digest_size, digest_hex);
	printf("%s: %s\n", path, digest_hex);
#endif

	close(fd);
}

void
recurse(char *basePath)
{
	char path[1024];
	struct dirent *dp;
	struct stat sbuf;

	DIR *dir = opendir(basePath);
	if (!dir) {
		fprintf(stderr, "ERROR: Failed to open dir %s: %m", basePath);
		return;
	}

	while ((dp = readdir(dir)) != NULL) {
		if (strcmp(dp->d_name, ".") == 0) continue;
		if (strcmp(dp->d_name, "..") == 0) continue;

		strcpy(path, basePath);
		strcat(path, "/");
		strcat(path, dp->d_name);

		// Recuse into directories, handle files, skip symlinks
		lstat(path, &sbuf);
		if (S_ISDIR(sbuf.st_mode))
			recurse(path);
		else if (S_ISREG(sbuf.st_mode))
			handle_file(path);
	}

	closedir(dir);
}

int
main(int argc, char* argv[])
{
	if (argc != 2) {
		fprintf(stderr, "USAGE: %s /path/to/verify\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	recurse(argv[1]);
	
	return EXIT_SUCCESS;
}
